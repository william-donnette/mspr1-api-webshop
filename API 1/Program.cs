using System.Security.Claims;

using Keycloak.AuthServices.Authentication;

using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Http.Json;
using static Microsoft.Extensions.DependencyInjection.AuthorizationConstants.Policies;


var builder = WebApplication.CreateBuilder(args);

var configuration = builder.Configuration;
var services = builder.Services;

services.AddApplicationSwagger(configuration)
        .AddAuth(configuration);

services.Configure<JsonOptions>(opts =>
{
    opts.SerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
    opts.SerializerOptions.WriteIndented = true;
});

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app
    .UseHttpsRedirection()
    .UseApplicationSwagger(configuration)
    .UseAuthentication()
    .UseAuthorization();

app.MapControllers();

app.Run();