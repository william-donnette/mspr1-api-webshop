﻿using Newtonsoft.Json;
using System.Reflection.Metadata;
using System;

namespace API_1.DMO
{
    public class Customer
    {
        public DateTime createdAt { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int id { get; set; }
        public string email { get; set; }

        public Address address { get; set; }
        public Profile profile { get; set; }
        public Company company { get; set; }
        public Order[] orders { get; set; }


        [JsonConstructor]
        public Customer(DateTime createdAt, string name, string username, string firstName, string lastName, int id, string email, object? address, object? profile, object? company, Order[] orders)
        {
            this.createdAt = createdAt;
            this.name = name;
            this.username = username;
            this.firstName = firstName;
            this.lastName = lastName;
            this.id = id;
            this.email = email;
            this.orders = orders;

            Type addressType = address.GetType();

            if (addressType.Equals(typeof(Newtonsoft.Json.Linq.JArray))) {

                Newtonsoft.Json.Linq.JArray addr = (Newtonsoft.Json.Linq.JArray)address;

                this.address = new Address(addr.Value<string>(0), addr.Value<string>(1));

            } else if (addressType.Equals(typeof(Newtonsoft.Json.Linq.JObject))) {

                Newtonsoft.Json.Linq.JObject addr = (Newtonsoft.Json.Linq.JObject)address;

                this.address = addr.ToObject<Address>();

            } else if (addressType == typeof(Address)) { 

                this.address = (Address)address;

            } else {

                this.address = new Address("Nothing Found", "Nothing Found");

            }

            Type profileType = profile.GetType();

            if (profileType.Equals(typeof(Newtonsoft.Json.Linq.JArray)))
            {
                Newtonsoft.Json.Linq.JArray prof = (Newtonsoft.Json.Linq.JArray)profile;

                this.profile = new Profile(prof.Value<string>(0), prof.Value<string>(1));

            }
            else if (profileType.Equals(typeof(Newtonsoft.Json.Linq.JObject)))
            {
                Newtonsoft.Json.Linq.JObject prof = (Newtonsoft.Json.Linq.JObject)profile;

                this.profile = prof.ToObject<Profile>();

            }
            else if (profileType == typeof(Profile))
            {
                this.profile = (Profile)profile;
            }
            else
            {

                this.profile = new Profile("Nothing Found", "Nothing Found");

            }

            Type companyType = company.GetType();

            if (companyType.Equals(typeof(string)))
            {
                string comp = (string)company;
                this.company = new Company(comp);

            }
            else if (companyType.Equals(typeof(Newtonsoft.Json.Linq.JObject)))
            {
                Newtonsoft.Json.Linq.JObject comp = (Newtonsoft.Json.Linq.JObject)company;

                this.company = comp.ToObject<Company>();

            }
            else if (companyType == typeof(Company)) {
                this.company = (Company)company;
            }
            else
            {

                this.company = new Company("Nothing Found");
            }

        }


        public Customer() { }
    }
}
