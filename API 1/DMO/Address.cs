﻿using Newtonsoft.Json;

namespace API_1.DMO
{
    public class Address
    {


        public string postalCode { get; set; }
        public string city { get; set; }

        [JsonConstructor]
        public Address(string postalCode, string city)
        {
            this.postalCode = postalCode;
            this.city = city;
        }

        public Address()
        {
        }
    }
}