﻿using Newtonsoft.Json;

namespace API_1.DMO
{
    public class Product
    {
        public DateTime createdAt { get; set; }

        public string name { get; set; }

        public int stock { get; set; }

        public Details details { get; set; }

        public int id { get; set; }

        public Product(DateTime createdAt, string name, int stock, Details details, int id)
        {
            this.createdAt = createdAt;
            this.name = name;
            this.stock = stock;
            this.details = details;
            this.id = id;
        }

        [JsonConstructor]
        public Product() { }
    }
}
