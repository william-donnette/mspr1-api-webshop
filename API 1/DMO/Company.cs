﻿using Newtonsoft.Json;

namespace API_1.DMO
{
    public class Company
    {
        public string companyName { get; set; }

        public Company(string companyName)
        {
            this.companyName = companyName;
        }

        [JsonConstructor]
        public Company() { }

    }
}