﻿using Newtonsoft.Json;

namespace API_1.DMO
{
    public class Profile
    {
        public string firstName { get; set; }
        public string lastName { get; set; }

        [JsonConstructor]
        public Profile(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Profile() { }
    }
}