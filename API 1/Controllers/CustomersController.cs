using API_1.DMO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CustomersController : ControllerBase
    {
        [HttpGet()]
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();

            var webRequest = new HttpRequestMessage(HttpMethod.Get, "https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/");

            var httpClient = new HttpClient();

            var response = httpClient.Send(webRequest);

            if (!response.IsSuccessStatusCode)
            {
                return customerList;
            }

            using var reader = new StreamReader(response.Content.ReadAsStream());
            string responseJson = reader.ReadToEnd();

            customerList = JsonConvert.DeserializeObject<List<Customer>>(responseJson);

             return customerList;


        }

        [HttpGet("{id}")]
        public Customer? GetCustomer(int id)
        {
            var webRequest = new HttpRequestMessage(HttpMethod.Get, ("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/" + id.ToString()) );

            var httpClient = new HttpClient();

            var response = httpClient.Send(webRequest);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            using var reader = new StreamReader(response.Content.ReadAsStream());
            string responseJson = reader.ReadToEnd();
            
            Customer customer = JsonConvert.DeserializeObject<Customer>(responseJson);

            return customer;
        }

        [HttpGet("{id}/orders")]
        public List<Order> GetCustomerOrders(int id)
        {
            List<Order> orderList = new List<Order>();

            var webRequest = new HttpRequestMessage(HttpMethod.Get, ("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/" + id.ToString() + "/orders") );

            var httpClient = new HttpClient();

            var response = httpClient.Send(webRequest);
            
            if (!response.IsSuccessStatusCode)
            {
                return orderList;
            }


            using var reader = new StreamReader(response.Content.ReadAsStream());
            string responseJson = reader.ReadToEnd();

            orderList = JsonConvert.DeserializeObject<List<Order>>(responseJson);

            return orderList;
        }

        [HttpGet("{idCustomer}/orders/{idOrder}")]
        public Order GetCustomerOrder(int idCustomer, int idOrder)
        {

            var webRequest = new HttpRequestMessage(HttpMethod.Get, ("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/" + idCustomer.ToString() + "/orders/" + idOrder.ToString()) );

            var httpClient = new HttpClient();

            var response = httpClient.Send(webRequest);
            
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            using var reader = new StreamReader(response.Content.ReadAsStream());
            string responseJson = reader.ReadToEnd();

            Order order = JsonConvert.DeserializeObject<Order>(responseJson);

            return order;

        }

        [HttpGet("{idCustomer}/orders/{idOrder}/products")]
        public List<Product> GetCustomerOrderProducts(int idCustomer, int idOrder)
        {
            List<Product> productList = new List<Product>();

            var webRequest = new HttpRequestMessage(HttpMethod.Get, ("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/" + idCustomer.ToString() + "/orders/" + idOrder.ToString() + "/products/") );

            var httpClient = new HttpClient();

            var response = httpClient.Send(webRequest);

            if (!response.IsSuccessStatusCode)
            {
                return productList;
            }


            using var reader = new StreamReader(response.Content.ReadAsStream());
            string responseJson = reader.ReadToEnd();

            productList = JsonConvert.DeserializeObject<List<Product>>(responseJson);

            return productList;
        }


        [HttpGet("version")]
        public int GetVersion()
        {
            return 6;

        }
    }
}
