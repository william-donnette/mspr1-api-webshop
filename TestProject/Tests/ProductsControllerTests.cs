﻿using API_1.Controllers;
using API_1.DMO;

namespace API_1.Tests
{
    [TestFixture]
    public class ProductsControllerTests
    {
        [Test]
        public void GetAllProducts_ReturnsListOfProducts()
        {
            // Arrange
            var controller = new ProductsController();

            // Act
            var result = controller.GetAllProducts();

            // Assert
            Assert.IsInstanceOf<List<Product>>(result);
            Assert.That(result.Count, Is.GreaterThan(0));
        }

        [Test]
        public void GetProduct_WithValidId_ReturnsProduct()
        {
            // Arrange
            var controller = new ProductsController();
            int productId = 1;

            // Act
            var result = controller.GetProduct(productId);


            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<Product>(result);
            Assert.That(result.id, Is.EqualTo(productId));
        }

        [Test]
        public void GetProduct_WithInvalidId_ReturnsNotFound()
        {
            // Arrange
            var controller = new ProductsController();
            int invalidProductId = -1;

            // Act
            var result = controller.GetProduct(invalidProductId);

            // Assert
            Assert.IsNull(result);
        }
    }
}
