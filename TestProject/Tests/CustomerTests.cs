﻿using API_1.DMO;
using Newtonsoft.Json.Linq;

namespace API_1.Tests
{
    [TestFixture]
    public class CustomerTests
    {
        [Test]
        public void CustomerConstructor_SetProperties_Success()
        {
            // Arrange
            DateTime createdAt = DateTime.Now;
            string name = "Jean osef";
            string username = "jeanosef";
            string firstName = "jean";
            string lastName = "osef";
            int id = 123;
            string email = "jeanosef@example.com";
            Order[] orders = new Order[] { new Order(), new Order() };
            Address address = new Address("12345", "villeosef");
            Profile profile = new Profile("jean", "osef");
            Company company = new Company("companyosef");

            // Act
            Customer customer = new Customer(createdAt, name, username, firstName, lastName, id, email, address, profile, company, orders);

            // Assert
            Assert.That(customer.createdAt, Is.EqualTo(createdAt));
            Assert.That(customer.name, Is.EqualTo(name));
            Assert.That(customer.username, Is.EqualTo(username));
            Assert.That(customer.firstName, Is.EqualTo(firstName));
            Assert.That(customer.lastName, Is.EqualTo(lastName));
            Assert.That(customer.id, Is.EqualTo(id));
            Assert.That(customer.email, Is.EqualTo(email));
            Assert.That(customer.orders.Length, Is.EqualTo(orders.Length));
            Assert.That(customer.address.postalCode, Is.EqualTo(address.postalCode));
            Assert.That(customer.address.city, Is.EqualTo(address.city));
            Assert.That(customer.profile.lastName, Is.EqualTo(profile.lastName));
            Assert.That(customer.profile.firstName, Is.EqualTo(profile.firstName));
            Assert.That(customer.company.companyName, Is.EqualTo(company.companyName));
        }

        [Test]
        public void CustomerConstructor_InvalidAddressObject_SetAddressToDefaultValues()
        {
            // Arrange
            DateTime createdAt = DateTime.Now;
            string name = "Jean osef";
            string username = "jeanosef";
            string firstName = "jean";
            string lastName = "osef";
            int id = 123;
            string email = "jeanosef@example.com";
            Order[] orders = new Order[] { new Order(), new Order() };
            object invalidAddress = "invalid address object";
            Profile profile = new Profile("jean", "osef");
            Company company = new Company("companyosef");

            // Act
            Customer customer = new Customer(createdAt, name, username, firstName, lastName, id, email, invalidAddress, profile, company, orders);

            // Assert
            Assert.That(customer.address.postalCode, Is.EqualTo("Nothing Found"));
            Assert.That(customer.address.city, Is.EqualTo("Nothing Found"));
        }

        [Test]
        public void CustomerConstructor_InvalidProfileObject_SetProfileToDefaultValues()
        {
            // Arrange
            DateTime createdAt = DateTime.Now;
            string name = "Jean osef";
            string username = "jeanosef";
            string firstName = "jean";
            string lastName = "osef";
            int id = 123;
            string email = "jeanosef@example.com";
            Order[] orders = new Order[] { new Order(), new Order() };
            Address address = new Address("12345", "villeosef");
            object invalidProfile = "invalid profile object";
            Company company = new Company("companyosef");

            // Act
            Customer customer = new Customer(createdAt, name, username, firstName, lastName, id, email, address, invalidProfile, company, orders);

            // Assert
            Assert.That(customer.profile.lastName, Is.EqualTo("Nothing Found"));
            Assert.That(customer.profile.firstName, Is.EqualTo("Nothing Found"));
        }

        [Test]
        public void Customer_WithInvalidAddressType_ReturnsNothingFoundAddress()
        {
            // Arrange
            var invalidAddress = "invalid address";
            var customer = new Customer(
                createdAt: new DateTime(2022, 1, 1),
                name: "Jean osef",
                username: "josef",
                firstName: "Jean",
                lastName: "osef",
                id: 1,
                email: "josef@example.com",
                address: invalidAddress,
                profile: new Profile(),
                company: new Company(),
                orders: new Order[0]
            );

            // Act
            var actualAddress = customer.address;

            // Assert
            var expectedAddress = new Address("Nothing Found", "Nothing Found");
            Assert.That(actualAddress.postalCode, Is.EqualTo(expectedAddress.postalCode));
            Assert.That(actualAddress.city, Is.EqualTo(expectedAddress.city));
        }

        [Test]
        public void Customer_WithValidAddress_ReturnsCorrectAddress()
        {
            // Arrange
            var addressJson = JArray.Parse(@"[""12345"", ""villeosef""]");
            var customer = new Customer(
                createdAt: new DateTime(2022, 1, 1),
                name: "Jean osef",
                username: "josef",
                firstName: "Jean",
                lastName: "osef",
                id: 1,
                email: "josef@example.com",
                address: addressJson,
                profile: new Profile(),
                company: new Company(),
                orders: new Order[0]
            );

            // Act
            var actualAddress = customer.address;

            // Assert
            var expectedAddress = new Address("12345", "villeosef");
            Assert.That(actualAddress.postalCode, Is.EqualTo(expectedAddress.postalCode));
            Assert.That(actualAddress.city, Is.EqualTo(expectedAddress.city));
        }

        [Test]
        public void Customer_WithValidProfile_ReturnsCorrectProfile()
        {
            // Arrange
            var profileJson = JArray.Parse(@"[""jean"", ""osef""]");
            var customer = new Customer(
                createdAt: new DateTime(2022, 1, 1),
                name: "Jean osef",
                username: "josef",
                firstName: "Jean",
                lastName: "osef",
                id: 1,
                email: "josef@example.com",
                address: new Address(),
                profile: profileJson,
                company: new Company(),
                orders: new Order[0]
            );

            // Act
            var actualProfile = customer.profile;

            // Assert
            var expectedProfile = new Profile("jean", "osef");
            Assert.That(actualProfile.firstName, Is.EqualTo(expectedProfile.firstName));
            Assert.That(actualProfile.lastName, Is.EqualTo(expectedProfile.lastName));
        }
    }
}