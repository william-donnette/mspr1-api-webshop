﻿using API_1.Controllers;
using API_1.DMO;
using NUnit.Framework;
using System.Collections.Generic;

namespace API_1.Tests
{
    public class CustomersControllerTests
    {
        private CustomersController _controller;

        [SetUp]
        public void Setup()
        {
            _controller = new CustomersController();
        }

        [Test]
        public void GetAllCustomers_ReturnsCustomerList()
        {
            // Arrange

            // Act
            List<Customer> result = _controller.GetAllCustomers();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(List<Customer>), result);
        }

        [Test]
        public void GetCustomer_WithValidId_ReturnsCustomer()
        {
            // Arrange
            int id = 1;

            // Act
            Customer result = _controller.GetCustomer(id);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(Customer), result);
            Assert.That(result.id, Is.EqualTo(id));
        }

        [Test]
        public void GetCustomer_WithInvalidId_ReturnsNull()
        {
            // Arrange
            int id = -1;

            // Act
            Customer result = _controller.GetCustomer(id);

            // Assert
            Assert.IsNull(result);
        }

        [Test]
        public void GetCustomerOrders_WithValidId_ReturnsOrderList()
        {
            // Arrange
            int id = 1;

            // Act
            List<Order> result = _controller.GetCustomerOrders(id);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(List<Order>), result);
        }

        [Test]
        public void GetCustomerOrders_WithInvalidId_ReturnsEmptyList()
        {
            // Arrange
            int id = -1;

            // Act
            List<Order> result = _controller.GetCustomerOrders(id);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Count, Is.EqualTo(0));
        }

        [Test]
        public void GetCustomerOrder_WithValidIds_ReturnsOrder()
        {
            // Arrange
            int customerId = 1;
            int orderId = 1;

            // Act
            Order result = _controller.GetCustomerOrder(customerId, orderId);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(Order), result);
            Assert.That(result.customerId, Is.EqualTo(customerId));
            Assert.That(result.id, Is.EqualTo(orderId));
        }

        [Test]
        public void GetCustomerOrder_WithInvalidIds_ReturnsNull()
        {
            // Arrange
            int customerId = -1;
            int orderId = -1;

            // Act
            Order result = _controller.GetCustomerOrder(customerId, orderId);

            // Assert
            Assert.IsNull(result);
        }

        [Test]
        public void GetCustomerOrderProducts_WithValidIds_ReturnsProductList()
        {
            // Arrange
            int customerId = 1;
            int orderId = 1;

            // Act
            List<Product> result = _controller.GetCustomerOrderProducts(customerId, orderId);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOf(typeof(List<Product>), result);
        }

        [Test]
        public void GetCustomerOrderProducts_WithInvalidIds_ReturnsEmptyList()
        {
            // Arrange
            int customerId = -1;
            int orderId = -1;

            // Act
            List<Product> result = _controller.GetCustomerOrderProducts(customerId, orderId);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Count, Is.EqualTo(0));
        }



    }
}
