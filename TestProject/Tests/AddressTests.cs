﻿using NUnit.Framework;
using API_1.DMO;

namespace API_1.Tests
{
    [TestFixture]
    public class AddressTests
    {

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void AddressConstructor_SetProperties_Success()
        {
            // Arrange
            string postalCode = "12345";
            string city = "osef";

            // Act
            Address address = new Address(postalCode, city);

            // Assert
            Assert.That(address.postalCode, Is.EqualTo(postalCode));
            Assert.That(address.city, Is.EqualTo(city));
        }

        [Test]
        public void PostalCode_SetValidValue_Success()
        {
            // Arrange
            string postalCode = "12345";
            string city = "osef1";
            Address address = new Address(postalCode, city);

            // Act
            address.postalCode = "54321";

            // Assert
            Assert.That(address.postalCode, Is.EqualTo("54321"));
        }

        [Test]
        public void City_SetValidValue_Success()
        {
            // Arrange
            string postalCode = "12345";
            string city = "osef1";
            Address address = new Address(postalCode, city);

            // Act
            address.city = "osef2";

            // Assert
            Assert.That(address.city, Is.EqualTo("osef2"));
        }
    }
}